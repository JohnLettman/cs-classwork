/*
FILENAME:		main.c
PROJECT NAME:	Project 4
TITLE:			Number conversion and normalization	(without the standard library)		
PROGRAMMER:		John P. Lettman <jlettman@acm.org>
CLASS:			Computer Information Systems, Mr. Fuchs
DATE:			20 Feb 2013
*/

#include "main.h"

// #####################
// # Private variables #
// #####################

unsigned char outputBase = 10;


// #############################
// # Standard helper functions #
// #############################

int iputs(void* string)
{
	unsigned int amount = istrlen(string);
	return write(stdout, string, amount);
}

int iputc(char character)
{
	return write(stdout, &character, 1);
}

void iitoar(ultraint value, bool negative, char *buffer, unsigned char radix)
{
	static char digits[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    unsigned int bPos = 0;

    do
    {
        buffer[bPos++] = digits[value % radix];
        value /= radix;
    } while (value);

    if (negative)
    {
    	buffer[bPos++] = '-';
    }

    buffer[bPos] = '\0';

    // String reverse for masochists
    istrrev(buffer);
}

void istrrev(char *p)
{
  char *q = p;
  while(q && *q) ++q;
  for(--q; p < q; ++p, --q)
    *p = *p ^ *q,
    *q = *p ^ *q,
    *p = *p ^ *q;
}

unsigned int istrlen(char* string)
{
	unsigned int accumulator = 0;

	// Increment accumulator, 
	// check if that position in the string
	// equals NULL (if it does, break the loop)
	while(string[accumulator] != '\0') accumulator++;

	return accumulator;
}

ultraint pow(ultraint a, ultraint b)
{
	if(b == 0) return 1;

	ultraint result = 0;
	for(; b > 0; b--)
	{
		result += a;
	}

	return result;
}


// ############################
// # Static private functions #
// ############################

static inline void help()
{
	iputs("Usage: convert (-)(0)[o/b/h/x/");
	iputc(CUSTOM_BASE_DELIMITER);
	iputs("[base]");
	iputc(CUSTOM_BASE_DELIMITER);
	iputs("][number]...\n"
		"Normalize numbers of any base to base-10 (" ARCHITECTURE_STRING " support).\n\n"
		"(-)\t\t- optional negative sign\n"
		"(0)\t\t- optional zero prefix (for standards conformance)\n"
		"[o/b/h/x/...]\t- identifier for input base\n"
		"[number]\t- input number of input base\n\n"

		"Examples:\n\n"
		"convert 0xFF\t\t- converts hex FF to base-10\n"
		"convert 0b10101\t\t- converts binary 10101 to base-10\n"
		"convert 0|3|012001\t- converts base-3 (trinary) 012001 to base-10\n"
		"convert -xFF\t\t- converts hex negative FF to base-10\n\n"

		"Written by John P. Lettman, overthrow reality.\n"
		"Compiled on " __DATE__ " at " __TIME__ ".\n");
}

#ifdef COLOR_MODE
void errorMessage(char* fString, unsigned int fLocation, char* fMessage)
{
	const char colorPointer[] = "\033[1;32m^\033[0m";

	char _pointer[fLocation + 13];
	for(unsigned int i = 0; i < fLocation; i++)
	{
		_pointer[i] = ' ';
	}

	for(unsigned int i = fLocation; i < fLocation + 15; i++)
	{
		_pointer[i] = colorPointer[i - fLocation];
	}

	iputs("\n\033[1;31merror:\033[0m ");
	iputs(fMessage);
	iputc('\n');
	iputs(fString);
	iputc('\n');
	iputs(_pointer);
	iputs("\n\n");
}
#else
void errorMessage(char* fString, unsigned int fLocation, char* fMessage)
{
	char _pointer[fLocation + 1];
	_pointer[fLocation] = '^';
	_pointer[fLocation + 1] = '\0';

	iputs("\nerror: ");
	iputs(fMessage);
	iputc('\n');
	iputs(fString);
	iputc('\n');
	iputs(_pointer);
	iputs("\n\n");
}
#endif

#define CHAR2DIGIT_ERROR 255

static unsigned char char2digit(char character)
{
	if(character > 0x2F && character < 0x3A)
	{
		return character - 0x30;
	} 
	else if(character > 0x40 && character < 0x5B)
	{
		return character - 0x41 + 10;
	}
	else if(character > 0x60 && character < 0x7B)
	{
		return character - 0x61 + 10;
	}
	else
	{
		return CHAR2DIGIT_ERROR;
	}
}




// ####################
// # Public functions #
// ####################

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		help();
		return 1;
	}

	for(int i = 1; i < argc; i++)
	{
		normalize(argv[i]);
	}

	return 0;
}

void normalize(char* string)
{
	ultraint value = 0;
	unsigned char base = 0;

	bool isNegative = false;
	char* stringPosition = string;
	char* stringEnd = string + istrlen(string) - 1;

	if(*stringPosition == '-')
	{
		isNegative = true;
		stringPosition++;
	}

	if(*stringPosition == '0')
	{
		stringPosition++;
	}

	unsigned char _digit;
	switch(*stringPosition++)
	{
		case 'o':
			base = 8;
			break;

		case 'x':
		case 'h':
			base = 16;
			break;

		case 'b':
			base = 2;
			break;

		case CUSTOM_BASE_DELIMITER:
			while(*stringPosition != CUSTOM_BASE_DELIMITER)
			{
				_digit = *stringPosition - '0';

				if(_digit > 9 || _digit < 0)
				{
					errorMessage(string, (ultraint) stringPosition,
							"Unexpected symbol. Expected a base-10 digit defining the input base.");
					return;
				}
				else
				{
					base *= 10;
					base += _digit;
				}

				stringPosition++;
			}

			if(base > 36)
			{
				errorMessage(string, (ultraint) stringPosition,
						"Invalid input base. The maximum feasible base is 36 (all letters and numbers).");
			}

			stringPosition++;
			break;

		case ':':
			outputBase = 0;

			while(*stringPosition != '\0')
			{
				_digit = *stringPosition - '0';

				if(_digit > 9 || _digit < 0)
				{
					errorMessage(string, (ultraint) stringPosition,
							"Unexpected symbol. Expected a base-10 digit defining the new output base.");
					return;
				}
				else
				{
					outputBase *= 10;
					outputBase += _digit;
				}

				stringPosition++;
			}

			if(base > 36)
			{
				outputBase = 10; // reset for good measure
				errorMessage(string, (ultraint) stringPosition,
						"Invalid output base. The maximum feasible base is 36 (all letters and numbers).");
			}

			return;
			break;

		default:
			errorMessage(string, (ultraint) stringPosition - (ultraint) string - 1,
					"Invalid base number identifier. Expected "
					"o = Octal, x/h = Hexidecimal, b = Binary, or a Variadic Base.");
			return;
			break;
	}

	unsigned char digit;
	char* backwardsTracker = stringEnd;
	while(backwardsTracker >= stringPosition)
	{
		digit = char2digit(*backwardsTracker);

		if(digit == CHAR2DIGIT_ERROR || digit > base - 1)
		{
			errorMessage(string, (ultraint) backwardsTracker - (ultraint) string,
					"Unexpected symbol. Expected a digit relevant to the base.");

			return;
		}

		value += digit * pow(base, (ultraint) stringEnd - (ultraint) backwardsTracker);
		backwardsTracker--;
	}

	char buffer[IITOA_BUFFER_SIZE];
	iitoar(value, isNegative, buffer, outputBase);

	char baseBuffer[3];
	iitoar(base, false, baseBuffer, 10);

	char opBaseBuffer[3];
	iitoar(outputBase, false, opBaseBuffer, 10);

#ifdef COLOR_MODE
	iputs("(base ");
	iputs(baseBuffer);
	iputs(") \033[1;37m");
	iputs(string);
	iputs("\033[0m = (base ");
	iputs(opBaseBuffer);
	iputs(") \033[1;32m");
	iputs(buffer);
	iputs("\033[0m\n");
#else
	iputs("(base ");
	iputs(baseBuffer);
	iputs(") ");
	iputs(string);
	iputs(" = (base ");
	iputs(opBaseBuffer);
	iputs(") ");
	iputs(buffer);
	iputs("\n");
#endif
}
