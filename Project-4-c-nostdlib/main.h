/*
FILENAME:		main.h
PROJECT NAME:	Project 4
TITLE:			Number conversion and normalization	(without the standard library)		
PROGRAMMER:		John P. Lettman <jlettman@acm.org>
CLASS:			Computer Information Systems, Mr. Fuchs
DATE:			20 Feb 2013
*/

// NOTE: No inclusions what-so-ever
// NOTE: x86 and x86_64 only
// NOTE: 36

#define CUSTOM_BASE_DELIMITER '_'
#define COLOR_MODE


#if defined(__LP64__) || defined(_LP64)
# define _64BIT true
# define ARCHITECTURE_STRING "64-bit"
# define IITOA_BUFFER_SIZE 20 // number of digits in 2^64 = 20
#else
# define _64BIT false
# define ARCHITECTURE_STRING "32-bit"
# define IITOA_BUFFER_SIZE 10 // number of digits in 2^32 = 10
#endif

#define stdout 1
#define stderr 0


// #########
// # Types #
// #########

// C does not have booleans
typedef unsigned char bool;

#define false 0
#define true !false


#if _64BIT
typedef unsigned long long ultraint;
#else
typedef unsigned long ultraint;
#endif


// #######################################
// # Standard helper function prototypes #
// #######################################

int iputs(void* string);
int iputc(char character);
int write(int fd, void* buffer, unsigned int bytes);
void iitoar(ultraint value, bool negative, char *buffer, unsigned char radix);
void istrrev(char *p);
unsigned int istrlen(char* string);


// #############
// # Functions #
// #############

int main(int argc, char* argv[]);
void normalize(char* string);
